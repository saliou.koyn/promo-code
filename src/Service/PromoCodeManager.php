<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;


class PromoCodeManager
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * Find the promotional code corresponding to the given code
     *
     * @param string $code The promo code
     * @return array|null
     */
    public function findByCode(string $code)
    {
        $promoCodeList = $this->fetchAllPromoCode();
        foreach ($promoCodeList as $promoCodeValues) {
            if (isset($promoCodeValues['code']) && $promoCodeValues['code'] === $code) {
                return $promoCodeValues;
            }
        }

        return null;
    }

    /**
     *
     * @param array $promoCode
     * @return bool
     * @throws \Exception
     */
    public function isValid(array $promoCode): bool
    {
        if (empty($promoCode['endDate'])) {
            throw new \Exception('The endDate not defined ');
        }

        $promoCodeEndDate = new \DateTime($promoCode['endDate']);

        $now = new \DateTime("now");
        // If $promoCodeEndDate is equal to $now => the code is valid
        $now->sub(new \DateInterval('P1D'));

        return $promoCodeEndDate > $now;
    }

    /**
     *
     * @param string $code
     * @return array
     */
    public function findOffersByCode(string $code)
    {
        $offers = $this->fetchOffers();
        $compatibleOffers = [];
        foreach ($offers as $offerValues) {
            if (in_array($code, $offerValues['validPromoCodeList'])) {
                $compatibleOffers[] = $offerValues;
            }
        }

        return $compatibleOffers;
    }

    /**
     * Fetch All promo code from the API
     *
     * @return array
     * @throws \Exception
     */
    public function fetchAllPromoCode()
    {
        $url = 'https://601025826c21e10017050013.mockapi.io/ekwatest/promoCodeList';
        $response = $this->client->request(
            'GET',
            $url
        );

        $statusCode = $response->getStatusCode();
        if ($statusCode === 200) {
            return $response->toArray();
        } else {
            // log and throw exception
            throw new \Exception(sprintf('PromoCodeList Api error. statusCode : %s', $statusCode));
        }
    }

    /**
     * Fetch all offers from the API
     *
     * @return array
     * @throws \Exception
     */
    public function fetchOffers()
    {
        $url = 'https://601025826c21e10017050013.mockapi.io/ekwatest/offerList';
        $response = $this->client->request(
            'GET',
            $url
        );

        $statusCode = $response->getStatusCode();
        if ($statusCode === 200) {
            return $response->toArray();
        } else {
            // log and throw exception
            throw new \Exception(sprintf('OfferList Api error. statusCode : %s', $statusCode));
        }
    }
}