<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

use App\Service\PromoCodeManager;

class PromoCodeValidateCommand extends Command
{
    protected static $defaultName = 'promo-code:validate';
    protected static $defaultDescription = 'Checks the validity of a promo code, and retrieve the associated offers.';

    protected $defaultJsonFilePath = 'var/compatible_offers.json';

    /**
     *
     * @var PromoCodeManager
     */
    private $promoCodeManager;

    public function __construct(PromoCodeManager $promoCodeManager)
    {
        $this->promoCodeManager = $promoCodeManager;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('promocode', InputArgument::REQUIRED, 'The promo code to check.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $io = new SymfonyStyle($input, $output);

            $promoCode = $input->getArgument('promocode');
            $promoCodeData = $this->promoCodeManager->findByCode($promoCode);

            if (null === $promoCodeData) {
                $io->note('Promo code not found.');
                return Command::FAILURE;
            }

            if (! $this->promoCodeManager->isValid($promoCodeData)) {
                $io->note('The promotional code is not valid.');
                return Command::FAILURE;
            }
            $io->info('The promotional code is valid.');

            $compatibleOffers = $this->promoCodeManager->findOffersByCode($promoCode);
            if (empty($compatibleOffers)) {
                $io->note('No offer corresponds to this promo code.');
                return Command::FAILURE;
            }
            $io->info(sprintf('Number of compatible offers found : %d', count($compatibleOffers)));

            // Build the array to dump in json file
            $dataToDump = [
                'promoCode' => $promoCodeData['code'],
                'endDate' => $promoCodeData['endDate'],
                'discountValue' => $promoCodeData['discountValue'],
            ];

            $compatibleOfferTmp = [];
            foreach ($compatibleOffers as $values) {
                $compatibleOfferTmp[] = [
                    'name' => $values['offerName'] ?? '',
                    'type' => $values['offerType'] ?? ''
                ];
            }

            $dataToDump['compatibleOfferList'] = $compatibleOfferTmp;

            // Dumps data
            $this->dumpToJsonfile($dataToDump);

            $io->success(sprintf('compatible offers are generated in the following json file [ %s ].', $this->getFilePath()));

            return Command::SUCCESS;

        } catch (\Exception $ex) {
            $io->error(sprintf('An error has occurred : %s', $ex->getMessage()));
            return Command::FAILURE;
        }
    }

    public function dumpToJsonfile($data)
    {
        try {
            // Encode the data
            $jsonEncoder = new JsonEncoder();
            $encodedData = $jsonEncoder->encode($data, 'json');

            // Dumps data into a file
            $filePath = $this->getFilePath();
            $fs = new Filesystem();
            $fs->dumpFile($filePath, $encodedData);

        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    private function getFilePath()
    {
        return $this->defaultJsonFilePath;
    }
}